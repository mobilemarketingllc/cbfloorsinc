var $ = jQuery;
$(document).ready(function(){
    $('.onlycharallow input[type="text"]').attr('onKeyPress','return validateChar(event)');
    //$('.onlynumallow').attr('onKeyPress','return validateNum(event)');
    $('textarea').attr('onkeydown','return ValidateTextarea(event)');

    jQuery('.searchIcon .fl-icon').click(function(){
        jQuery('.searchModule').slideToggle();
    });

});

function validateChar(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 123)) {
        if (charCode == 8 || charCode == 32 || charCode == 9){
        console.log(charCode);
        return true;
        }
        else
        return false;
    } else
        return true;
}
function validateNum(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 43 || charCode == 40 || charCode == 41 || charCode == 9)
        return true;
        else
        return false;
    } else
        return true;
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (false);
    }                    
    return (true);
}